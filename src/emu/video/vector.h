#ifndef __VECTOR__
#define __VECTOR__

#define VECTOR_COLOR111(c) \
	MAKE_RGB(pal1bit((c) >> 2), pal1bit((c) >> 1), pal1bit((c) >> 0))

#define VECTOR_COLOR222(c) \
	MAKE_RGB(pal2bit((c) >> 4), pal2bit((c) >> 2), pal2bit((c) >> 0))

#define VECTOR_COLOR444(c) \
	MAKE_RGB(pal4bit((c) >> 8), pal4bit((c) >> 4), pal4bit((c) >> 0))

VIDEO_START( vector );
SCREEN_UPDATE( vector );

void vector_clear_list(void);
void vector_add_point(running_machine &machine, int x, int y, rgb_t color, int intensity);
void vector_add_clip(int minx, int miny, int maxx, int maxy);

void vector_set_flicker(float _flicker);
float vector_get_flicker(void);

void vector_set_newflicker(float _flicker);
float vector_get_newflicker(void);

void vector_set_afterimage(int newfade);
int vector_get_afterimage(void);

void vector_set_beamfade(int newfade);
int vector_get_beamfade(void);

void vector_set_newrenderer(int newrenderer);
int vector_get_newrenderer(void);

void vector_set_glowiterations(int newiterations);
int vector_get_glowiterations(void);

void vector_set_glowstrength(int newstrength);
int vector_get_glowstrength(void);

void vector_set_beam(float _beam);
float vector_get_beam(void);

#define VECTOR_BITMAP_ADDR(bitmap, x, y, rowlen) (bitmap + ((y) * rowlen) + (x))
#define VECTOR_BITMAP_BLEND(val1, val2) ((val1) > (val2) ? MIN((val1) + ((val2) >> 1), 255) : MIN((val2) + ((val1) >> 1), 255))
#define VECTOR_SWAP_INT(a, b) do {int tmp; tmp = a; a = b; b = tmp; } while(0);
#define VECTOR_IPART(X) ((int)(X))
#define VECTOR_ROUND(X) ((int)(((double)(X))+0.5))
#define VECTOR_FPART(X) (((double)(X))-(double)VECTOR_IPART(X))
#define VECTOR_RFPART(X) (1.0 - VECTOR_FPART(X))

void vector_drawpixel (int x1, int y1, UINT32 colrgb, int intensity);
void vector_drawaaline (int x1, int y1, UINT32 colrgb, int intensity);
void vector_drawaadot (int x1, int y1, int r, int g, int b, float intensity);
void vector_fade();
void vector_renderlinebitmap();
void vector_renderblurbitmap();
void vector_fastblur(int *src_r, int *src_g, int *src_b, int *dest_r, int *dest_g, int *dest_b);
void vector_makemipmaps();

#endif

